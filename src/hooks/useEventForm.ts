import moment from "moment";
import {useForm} from "react-hook-form";
import {apiClient} from "../lib/ApiClient.ts";
import {CalendarEvent, EventFormInputs, EventPayload} from "../types.ts";
import {yupResolver} from "@hookform/resolvers/yup";

import * as yup from "yup";

const eventSchema = yup.object().shape({
    title: yup.string().required("Title is required"),
    allDay: yup.boolean().required(),
    startAt: yup.date().required("Start date and time is required"),
    endAt: yup.date().required("End date and time is required"),
    recurring: yup.boolean().required(),
    recurringType: yup.string().when("recurring", {
        is: true,
        then: (schema) => schema.required("Recurring type is required"),
        otherwise: schema => schema.notRequired()
    }),
    recurringInterval: yup.number().when("recurring", {
        is: true,
        then: (schema) => schema.required("Recurring interval is required"),
        otherwise: schema => schema.notRequired()
    }),
    recurringEnds: yup.string().when("recurring", {
        is: true,
        then: (schema) => schema.required("Recurring ends is required"),
        otherwise: schema => schema.notRequired()
    }),
    occurrence: yup.number().when("recurringEnds", {
        is: "after",
        then: (schema) => schema.required("Occurrence is required"),
        otherwise: schema => schema.notRequired()
    })
});

const createEvent = async (data: EventPayload) => {
    return await apiClient.post<CalendarEvent>("/api/event/v1/events/create", data);
};

const updateEvent = async (id: number, data: EventPayload) => {
    return await apiClient.patch<CalendarEvent>(`/api/event/v1/events/${id}/update`, data);
};

interface UseEventFormProps {
    event?: CalendarEvent;
}

const useEventForm = ({event}: UseEventFormProps) => {

    const form = useForm<EventFormInputs>({
        defaultValues: {
            title: event?.title || "",
            description: event?.description || "",
            allDay: event?.all_day || false,
            startAt: moment(event?.start_at) || moment(),
            endAt: moment(event?.end_at) || moment().add(30, "minutes"),
            recurring: event?.recurring || false,
            recurringInterval: event?.recurring_interval || 1,
            recurringType: event?.recurring_type || "daily",
            recurringEnds: event?.recurring_until ? "on" : event?.occurrence ? "after" : "never",
            recurringUntil: moment(event?.recurring_until) || moment(),
            occurrence: event?.occurrence || 10
        },
        // @ts-ignore
        resolver: yupResolver(eventSchema)
    });

    const onSubmit = (
        data: EventFormInputs,
        onSuccess: (res: CalendarEvent) => void,
        onError: (err: Error) => void,
        update?: boolean
    ) => {
        const payload: EventPayload = {
            title: data.title,
            description: data.description,
            all_day: data.allDay,
            start_at: data.startAt.toISOString(),
            end_at: data.endAt.toISOString(),
            recurring: data.recurring
        };
        if (data.recurring) {
            payload.recurring_interval = data.recurringInterval;
            payload.recurring_type = data.recurringType;
            if (data.recurringEnds === "on") payload.recurring_until = data.recurringUntil.toISOString();
            if (data.recurringEnds === "after") payload.occurrence = data.occurrence;
        }
        if (update && event) {
            updateEvent(event?.id, payload)
                .then((res) => {
                    // @ts-expect-error ts-migrate(2531) FIXME: Object type not detected
                    onSuccess(res);
                })
                .catch((err) => onError(err));
        } else if (!update) {
            createEvent(payload)
                .then((res) => {
                    // @ts-expect-error ts-migrate(2531) FIXME: Object type not detected
                    onSuccess(res);
                })
                .catch((err) => onError(err));
        } else {
            throw new Error("Form action not defined");
        }
    };

    return {...form, onSubmit};
};
export default useEventForm;