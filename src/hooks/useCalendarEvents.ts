import {useState} from "react";
import {apiClient} from "../lib/ApiClient.ts";
import {CalendarEvent, PaginatedResponse} from "../types.ts";


const useGetEvents = () => {
    const endPoint = "/api/event/v1/events/";
    const [loading, setLoading] = useState(false);
    const [events, setEvents] = useState<CalendarEvent[]>([]);
    const [error, setError] = useState(null);

    const fetch = (start_date: Date, end_date: Date) => {
        setLoading(true);
        apiClient.get<PaginatedResponse<CalendarEvent>>(endPoint, {params: {start_date, end_date}})
            .then((res) => {
                setEvents(res.data.results);
                setLoading(false);
            }).catch((err) => {
            setError(err);
            setLoading(false);
        });
    };

    return {loading, eventResponses: events, error, fetch};
};

export default useGetEvents;

