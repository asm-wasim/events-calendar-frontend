import moment from "moment/moment";
import {useEffect, useState} from "react";
import {Event, SlotInfo} from "react-big-calendar";
import {CalendarEvent} from "../types.ts";
import useGetEvents from "./useCalendarEvents.ts";

const useWeekCalendar = () => {
    const [weekStart, setWeekStart] = useState(moment(moment().toDate()).startOf("week").toDate());
    const [weekEnd, setWeekEnd] = useState(moment(moment().toDate()).endOf("week").toDate());

    const {eventResponses, fetch: fetchEvents} = useGetEvents();
    const [events, setEvents] = useState<Event[]>([]);

    const [isUpdateForm, setIsUpdateForm] = useState(false);
    const [openEventForm, setOpenEventForm] = useState(false);
    const [selectedEvent, setSelectedEvent] = useState<CalendarEvent | undefined>();

    const getEvents = () => {
        fetchEvents(
            moment(weekStart).subtract(1, "day").toDate(),
            moment(weekEnd).add(1, "day").toDate()
        );
    };

    useEffect(() => {
        getEvents();
    }, [weekStart, weekEnd]);

    useEffect(() => {
        setEvents(convertEvents(eventResponses));
    }, [eventResponses]);
    const convertEvents = (events: CalendarEvent[]): Event[] => {
        return events.map((event) => {
            return {
                resource: event.id + event.start_at + event.end_at,
                title: event.title,
                description: event.description,
                allDay: event.all_day,
                start: moment(event.start_at).toDate(),
                end: moment(event.end_at).toDate()
            };
        });
    };

    const getSelectedCalendarEvent = (event: Event) => {
        return eventResponses.find((e) => e.id + e.start_at + e.end_at === event.resource);
    };

    const handleSelectEvent = (event: Event) => {
        const selectedEvent = getSelectedCalendarEvent(event);
        setSelectedEvent(selectedEvent);
        setIsUpdateForm(true);
        setOpenEventForm(true);
    };

    const handleSelectSlot = (slotInfo: SlotInfo) => {
        const start_time = moment(slotInfo.start).format("YYYY-MM-DDTHH:mm:ss");
        const end_time = moment(slotInfo.end).format("YYYY-MM-DDTHH:mm:ss");
        setSelectedEvent({
            id: 0,
            title: "",
            description: "",
            start_at: start_time,
            end_at: end_time,
            all_day: false,
            recurring: false,
            recurring_type: "daily",
            recurring_interval: 1,
            recurring_until: null,
            occurrence: null
        });
        setIsUpdateForm(false);
        setOpenEventForm(true);
    };

    const onRangeChange = (range: Date[] | { start: Date, end: Date }) => {
        if (Array.isArray(range)) {
            const start_date = range[0];
            const end_date = range[range.length - 1];
            setWeekStart(moment(start_date).startOf("week").toDate());
            setWeekEnd(moment(end_date).endOf("week").toDate());
        }
    };

    const onAddEvent = () => {
        setSelectedEvent(undefined);
        setIsUpdateForm(false);
        setOpenEventForm(true);
    };

    const closeEventForm = () => {
        setIsUpdateForm(false);
        setOpenEventForm(false);
    };

    return {
        events,
        isUpdateForm,
        openEventForm,
        selectedEvent,
        onAddEvent,
        handleSelectEvent,
        handleSelectSlot,
        setOpenEventForm,
        invalidateEvents: getEvents,
        onRangeChange,
        closeEventForm
    };
};

export default useWeekCalendar;