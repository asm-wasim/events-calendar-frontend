import {createTheme} from "@mui/material";

const theme = createTheme({
    palette: {
        primary: {
            main: "#1976d2"
        },
        secondary: {
            main: "#dc004e"
        },
        background: {
            default: "#f4f4f4"
        },
        text: {
            primary: "#333333",
            secondary: "#666666"
        }
    },
    typography: {
        fontFamily: "Roboto, sans-serif",
        h1: {
            fontSize: "2.5rem",
            fontWeight: 600
        },
        h2: {
            fontSize: "2rem",
            fontWeight: 500
        },
        h3: {
            fontSize: "1.75rem",
            fontWeight: 500
        },
        h4: {
            fontSize: "1.5rem",
            fontWeight: 500
        },
        h5: {
            fontSize: "1.25rem",
            fontWeight: 500
        },
        h6: {
            fontSize: "1rem",
            fontWeight: 500
        },
        subtitle1: {
            fontSize: "1rem",
            fontWeight: 400
        },
        subtitle2: {
            fontSize: "0.875rem",
            fontWeight: 400
        },
        body1: {
            fontSize: "1rem",
            fontWeight: 400
        },
        body2: {
            fontSize: "0.875rem",
            fontWeight: 400
        },
        button: {
            fontSize: "1rem",
            fontWeight: 500,
            textTransform: "capitalize"
        },
        caption: {
            fontSize: "0.75rem",
            fontWeight: 400
        },
        overline: {
            fontSize: "0.625rem",
            fontWeight: 400
        }
    },
    spacing: 4
});

export default theme;