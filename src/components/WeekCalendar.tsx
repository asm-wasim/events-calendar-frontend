import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import {Container, IconButton, Typography} from "@mui/material";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";

import moment from "moment";
import {useEffect, useMemo} from "react";
import {Calendar, momentLocalizer, ToolbarProps, Views} from "react-big-calendar";

import "react-big-calendar/lib/css/react-big-calendar.css";
import useWeekCalendar from "../hooks/useWeekCalendar.ts";
import EventFormModal from "./EventFormModal.tsx";

const localizer = momentLocalizer(moment);


const WeekCalendar = () => {
    const {
        events,
        isUpdateForm,
        openEventForm,
        selectedEvent,
        onAddEvent,
        handleSelectEvent,
        handleSelectSlot,
        onRangeChange,
        invalidateEvents,
        closeEventForm
    } = useWeekCalendar();

    const {components} = useMemo(() => {
        return {
            components: {
                toolbar: (props: ToolbarProps) => {
                    return (
                        <Box sx={{mb: 2, display: "flex", justifyContent: "space-between"}}>
                            <Box>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    onClick={onAddEvent}
                                    sx={{mr: 10}}
                                >
                                    Add Event
                                </Button>
                                <IconButton onClick={() => props.onNavigate("PREV")}>
                                    <ChevronLeftIcon/>
                                </IconButton>
                                <Button variant={"outlined"} onClick={() => props.onNavigate("TODAY")}>
                                    Today
                                </Button>
                                <IconButton onClick={() => props.onNavigate("NEXT")}>
                                    <ChevronRightIcon/>
                                </IconButton>
                            </Box>
                            <Typography variant={"h4"} sx={{mb: 3}}>Week Calendar</Typography>
                        </Box>
                    );
                }
            }
        };
    }, []);

    useEffect(() => {
        console.log("Selected Event:", selectedEvent);
    }, [selectedEvent]);

    return (
        <Container maxWidth={"xl"} sx={{py: 6}}>
            <EventFormModal
                forUpdate={isUpdateForm}
                event={selectedEvent}
                open={openEventForm}
                handleClose={closeEventForm}
                onSubmitted={() => {
                    closeEventForm();
                    invalidateEvents();
                }}
            />
            <Calendar
                popup={true}
                defaultView={Views.WEEK}
                views={{week: true}}
                localizer={localizer}
                events={events}
                startAccessor="start"
                endAccessor="end"
                allDayAccessor="allDay"
                selectable={true}
                onSelectSlot={handleSelectSlot}
                onSelectEvent={handleSelectEvent}
                onRangeChange={onRangeChange}
                components={components}
            />
        </Container>
    );
};

export default WeekCalendar;

