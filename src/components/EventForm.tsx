import CancelIcon from "@mui/icons-material/Cancel";
import {
    Checkbox,
    FormControl,
    FormControlLabel,
    FormLabel,
    IconButton,
    MenuItem,
    Radio,
    RadioGroup,
    Select,
    TextField, Typography
} from "@mui/material";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import {DateTimePicker, LocalizationProvider} from "@mui/x-date-pickers";
import {AdapterMoment} from "@mui/x-date-pickers/AdapterMoment";
import React, {useEffect} from "react";
import {Controller} from "react-hook-form";
import useEventForm from "../hooks/useEventForm.ts";
import {CalendarEvent, EventFormInputs} from "../types.ts";

interface EventFormProps {
    forUpdate?: boolean;
    event?: CalendarEvent;
    onCanceled?: () => void;
    onSubmitted?: (res: CalendarEvent) => void;
}

const EventForm = (props: EventFormProps) => {
    const {
        control,
        watch,
        handleSubmit,
        onSubmit,
        formState: {isSubmitting, errors}
    } = useEventForm({event: props.event});

    const recurring = watch("recurring");
    const recurringEnds = watch("recurringEnds");
    const onSuccessfulSubmit = (res: CalendarEvent) => props.onSubmitted?.(res);
    const onFailedSubmit = (err: Error) => window.alert(err.message);
    const onFormSubmit = (data: EventFormInputs) => onSubmit(data, onSuccessfulSubmit, onFailedSubmit, props.forUpdate);

    useEffect(() => {
        console.log(errors);
    }, [errors]);

    return (
        <LocalizationProvider dateAdapter={AdapterMoment}>
            <Box sx={{height: 42, bgcolor: "#f2f4f6"}}>
                <IconButton sx={{position: "absolute", top: 0, right: 0}} onClick={props.onCanceled}>
                    <CancelIcon/>
                </IconButton>
            </Box>
            <form onSubmit={handleSubmit(onFormSubmit)}>
                <Box sx={{display: "flex", flexDirection: "column", gap: 3, p: 6}}>
                    <Controller
                        control={control}
                        name={"title"}
                        render={({field}) => (
                            <FormControl>
                                <TextField {...field} placeholder="Title"/>
                                <FormLabel error>{errors.title?.message}</FormLabel>
                            </FormControl>
                        )}
                    />
                    <Box sx={{display: "flex", flexDirection: "row", gap: 2}}>
                        <Controller
                            control={control}
                            name={"startAt"}
                            render={({field}) => (
                                <FormControl>
                                    <DateTimePicker {...field}/>
                                    <FormLabel error>{errors.startAt?.message}</FormLabel>
                                </FormControl>
                            )}
                        />
                        <Controller
                            control={control}
                            name={"endAt"}
                            render={({field}) => (
                                <FormControl>
                                    <DateTimePicker {...field}/>
                                    <FormLabel error>{errors.endAt?.message}</FormLabel>
                                </FormControl>
                            )}
                        />
                    </Box>
                    <Controller
                        control={control}
                        name={"allDay"}
                        render={({field}) => (
                            <FormControl>
                                <FormControlLabel
                                    label="All Day"
                                    control={
                                        <Checkbox
                                            defaultChecked={props.event?.all_day}
                                            {...field}
                                        />
                                    }
                                />
                                <FormLabel error>{errors.allDay?.message}</FormLabel>
                            </FormControl>
                        )}
                    />
                    <Controller
                        control={control}
                        name={"recurring"}
                        render={({field}) => (
                            <FormControl>
                                <FormControlLabel
                                    label="Recurring"
                                    control={
                                        <Checkbox
                                            defaultChecked={props.event?.recurring}
                                            {...field}
                                        />
                                    }
                                />
                                <FormLabel error>{errors.recurring?.message}</FormLabel>
                            </FormControl>
                        )}
                    />
                    {recurring && (
                        <>
                            <FormControl>
                                <Box sx={{display: "flex", flexDirection: "row", gap: 4, alignItems: "center"}}>
                                    <FormLabel>Repeat Every</FormLabel>
                                    <Controller
                                        control={control}
                                        name={"recurringInterval"}
                                        render={({field}) => (
                                            <FormControl>
                                                <TextField{...field} type="number"/>
                                            </FormControl>
                                        )}
                                    />
                                    <Controller
                                        control={control}
                                        name={"recurringType"}
                                        render={({field}) => (
                                            <FormControl>
                                                <Select {...field} >
                                                    <MenuItem value={"daily"}>Daily</MenuItem>
                                                    <MenuItem value={"weekly"}>Weekly</MenuItem>
                                                    <MenuItem value={"monthly"}>Monthly</MenuItem>
                                                    <MenuItem value={"yearly"}>Yearly</MenuItem>
                                                </Select>
                                            </FormControl>
                                        )}
                                    />
                                </Box>
                                <FormLabel error>{errors.recurringInterval?.message}</FormLabel>
                                <FormLabel error>{errors.recurringType?.message}</FormLabel>
                            </FormControl>
                            <FormControl>
                                <FormLabel>Ends</FormLabel>
                                <Controller
                                    control={control}
                                    name={"recurringEnds"}
                                    render={({field}) => (
                                        <FormControl>
                                            <RadioGroup
                                                row
                                                {...field}
                                                sx={{display: "flex", flexDirection: "column", gap: 1, pl: 3}}
                                            >
                                                <FormControlLabel value="never" control={<Radio/>} label="Never"/>
                                                <Box sx={{
                                                    display: "flex",
                                                    justifyContent: "space-between",
                                                    width: "80%"
                                                }}>
                                                    <FormControlLabel value="on" control={<Radio/>} label="On"/>
                                                    <Controller
                                                        control={control}
                                                        name={"recurringUntil"}
                                                        render={({field}) => (
                                                            <FormControl>
                                                                <DateTimePicker
                                                                    {...field}
                                                                    disabled={recurringEnds != "on"}
                                                                />
                                                            </FormControl>
                                                        )}
                                                    />
                                                </Box>
                                                <Box sx={{
                                                    display: "flex",
                                                    justifyContent: "space-between",
                                                    width: "80%"
                                                }}>
                                                    <FormControlLabel
                                                        value="after"
                                                        control={<Radio/>}
                                                        label="After (occurrences)"
                                                    />
                                                    <Controller
                                                        control={control}
                                                        name={"occurrence"}
                                                        render={({field}) => (
                                                            <FormControl>
                                                                <TextField
                                                                    {...field}
                                                                    type="number"
                                                                    disabled={recurringEnds != "after"}
                                                                />
                                                            </FormControl>
                                                        )}
                                                    />
                                                </Box>
                                            </RadioGroup>
                                        </FormControl>
                                    )}
                                />
                                <FormLabel error>{errors.recurringEnds?.message}</FormLabel>
                            </FormControl>
                        </>
                    )}
                    <FormControl>
                        <Controller
                            control={control}
                            name={"description"}
                            render={({field}) => (
                                <FormControl>
                                    <TextField {...field} placeholder="Note"/>
                                </FormControl>
                            )}
                        />
                        <FormLabel error>{errors.description?.message}</FormLabel>
                    </FormControl>
                    {(props.forUpdate && recurring) && (
                        <Typography variant="caption" color="error">
                            Updating recurring event is not supported.
                        </Typography>
                    )}
                    <Button type="submit" variant="contained" disabled={isSubmitting || (props.forUpdate && recurring)}>
                        {isSubmitting ? "Saving..." : "Save"}
                    </Button>
                </Box>

            </form>
        </LocalizationProvider>
    );
};

export default EventForm;