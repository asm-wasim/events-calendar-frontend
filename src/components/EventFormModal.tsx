import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import * as React from "react";
import {CalendarEvent} from "../types.ts";
import EventForm from "./EventForm.tsx";

const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 600,
    bgcolor: "background.paper",
    boxShadow: 8,
    borderRadius: 2
};

interface EventFormModalProps {
    forUpdate?: boolean;
    event?: CalendarEvent;
    open: boolean;
    handleClose: () => void;
    onSubmitted: (event: CalendarEvent) => void;
}

const EventFormModal = (props: EventFormModalProps) => {
    return (
        <Modal
            open={props.open}
            onClose={props.handleClose}
            slotProps={{backdrop: {invisible: true}}}
        >
            <Box sx={style}>
                <EventForm
                    forUpdate={props.forUpdate}
                    event={props.event}
                    onCanceled={props.handleClose}
                    onSubmitted={props.onSubmitted}
                />
            </Box>
        </Modal>
    );
};

export default EventFormModal;