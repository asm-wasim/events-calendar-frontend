export const DEMO_USER_TOKEN = process.env.DEMO_USER_TOKEN;
export const API_BASE_URL = process.env.API_ENDPOINT;

export const API_TIMEOUT = 60_000;
