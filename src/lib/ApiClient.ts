import axios from "axios";
import {API_BASE_URL, API_TIMEOUT, DEMO_USER_TOKEN} from "../constants.ts";

export const apiClient = axios.create({
    baseURL: API_BASE_URL,
    timeout: API_TIMEOUT
});

apiClient.interceptors.request.use((config) => {
    const access_token = DEMO_USER_TOKEN;
    if (access_token) config.headers.Authorization = `Token ${access_token}`;
    return config;
});
