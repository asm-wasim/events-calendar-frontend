import {Moment} from "moment/moment";

export interface PaginatedResponse<T> {
    count: number;
    next: string | null;
    previous: string | null;
    results: T[];
}

export interface CalendarEvent {
    id: number;
    title: string;
    description: string;
    all_day: boolean;
    start_at: string;
    end_at: string;
    recurring: boolean;
    recurring_type: string;
    recurring_interval: number;
    recurring_until: string | null;
    occurrence: number | null;
}

export interface EventFormInputs {
    title: string;
    description?: string;
    allDay: boolean;
    startAt: Moment;
    endAt: Moment;
    recurring: boolean;
    recurringType: string;
    recurringInterval: number;
    recurringEnds: "never" | "on" | "after";
    recurringUntil: Moment;
    occurrence: number;
}

export interface EventPayload {
    title: string;
    description?: string;
    all_day: boolean;
    start_at: string;
    end_at: string;
    recurring: boolean;
    recurring_type?: string;
    recurring_interval?: number;
    recurring_until?: string;
    occurrence?: number;
}

export interface EventUpdatePayload extends EventPayload {
    edit_type: "single" | "all" | "this_and_future";
}