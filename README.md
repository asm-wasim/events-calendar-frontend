# Events Calendar

This is a simple events calendar application built with Vite, React, and TypeScript.

## Features

- Users can view the events
    - Events are displayed in a weekly calendar view
    - Navigation buttons allow the user to move between weeks
    - Can back to the current week by clicking on the "Today" button
    - Events are displayed in the correct day and time slot
- User can create a new event
    - User can click on a day and time slot to open a modal to create a new event
    - User can enter a title, start date, end date, and notes for the event
    - Events can be all-day or have a specific start and end time
    - Events also can be recurring (e.g. every Monday, every weekday, etc.)
    - For recurring events, the user can choose the end date or the number of occurrences
- User can update an existing event
    - User can click on an existing event to open a modal to update the event
    - User can update the title, start date, end date, and notes for the event
    - User can only update the event if it's not recurring

## Getting Started

Clone repository with SSH:

```bash
git clone git@gitlab.com:asm-wasim/events-calendar-frontend.git
```

Or with HTTPS:

```bash
git clone https://gitlab.com/asm-wasim/events-calendar-frontend.git
```

Run the following commands to start the development server:

```bash
cd events-calendar-frontend
npm install
npm run dev
```

Environment variables are stored in a `.env` file at the root of the project. The following environment variables are
required:

```env
API_ENDPOINT='http://127.0.0.1:8000'
DEMO_USER_TOKEN='demo_user_token'
```
  
